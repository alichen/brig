//
//  MyMapView.m
//  Brig
//
//  Created by chenli on 16/2/25.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCTViewManager.h"
#import <MapKit/MapKit.h>

@interface MyMapManager : RCTViewManager

@end

@implementation MyMapManager

RCT_EXPORT_MODULE()

- (MKMapView *)view
{
  MKMapView *_mapview = [[MKMapView alloc] init];
  return _mapview;
}

RCT_EXPORT_VIEW_PROPERTY(pitchEnabled, BOOL)

//RCT_CUSTOM_VIEW_PROPERTY(region, MKCoordinateRegion, RCTMap)
//{
//  [view setRegion:json?[RCTConvert MKCoordinateRegion:json]:defaultView.region animated:YES]
//}

@end

@implementation RCTConvert(CoreLocation)

RCT_CONVERTER(CLLocationDegrees, CLLocationDegrees, doubleValue);
RCT_CONVERTER(CLLocationDistance, CLLocationDistance, doubleValue);

+ (CLLocationCoordinate2D)CLLocationCoordinate2D:(id)json
{
  json = [self NSDictionary:json];
  return (CLLocationCoordinate2D){
    [self CLLocationDegrees:json[@"latitude"]],
    [self CLLocationDegrees:json[@"longitude"]]
  };
}

@end

@implementation RCTConvert(MapKit)

+ (MKCoordinateSpan)MKCoordinateSpan:(id)json
{
  json = [self NSDictionary:json];
  return (MKCoordinateSpan){
    [self CLLocationDegrees:json[@"latitudeDelta"]],
    [self CLLocationDegrees:json[@"longitudeDelta"]]
  };
}


+ (MKCoordinateRegion)MKCoordinateRegion:(id)json
{
  return (MKCoordinateRegion){
    [self CLLocationCoordinate2D:json],
    [self MKCoordinateSpan:json]
  };
  
}

@end
