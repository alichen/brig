//
//  CalendarManager.m
//  Brig
//
//  Created by chenli on 16/2/23.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "CalendarManager.h"
#import "RCTBridge.h"
#import "RCTEventDispatcher.h"

@implementation CalendarManager

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(addEvent:(NSString *)name location:(NSString *)location)
{
  NSLog(@"hello from oc %@ at %@", name, location);
  [self calendarEventReminderReceived];
}

RCT_REMAP_METHOD(findEvents, resolver:(RCTPromiseResolveBlock)resolve rejector:(RCTPromiseRejectBlock)reject)
{
  //  NSArray *events = ...
  //  if(events){
  //    resolve(events);
  //  }else{
  //    reject(events);
  //  };
}

@synthesize bridge = _bridge;

- (void) calendarEventReminderReceived
{
  [self.bridge.eventDispatcher sendAppEventWithName:@"onReminder" body:@{@"name":@"respond from native event"}];
}



@end
