//
//  Todo.swift
//  Brig
//
//  Created by chenli on 16/2/23.
//  Copyright © 2016年 Facebook. All rights reserved.
//
import Foundation

@objc(Todo) class Todo: NSObject {
  var bridge:RCTBridge!
  @objc func addEvent(name:String,location:String){
    print("hello from swift %s at %s",name,location)
    self.triggerEvent()
    
  }
  
  func triggerEvent(){
    print(bridge.eventDispatcher.sendAppEventWithName("otherEvent",body:"respond from other event"))
  }
  
  @objc func findSomeThing(callback:RCTResponseSenderBlock){
    callback(["find result"])
  }
  
  @objc func findAll(name:String,resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock){
    if name != ""{
      resolve(name)
    }else{
      reject("fail","fail",nil)
    }
  }
}