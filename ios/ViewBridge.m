//
//  ViewBridge.m
//  Brig
//
//  Created by chenli on 16/2/25.
//  Copyright © 2016年 Facebook. All rights reserved.
//
#import "RCTBridgeModule.h"
#import "RCTViewManager.h"

@interface RCT_EXTERN_MODULE(MyLabel,RCTViewManager)
RCT_EXPORT_VIEW_PROPERTY(text, NSString *)
@end