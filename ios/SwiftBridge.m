//
//  TodoBridge.m
//  Brig
//
//  Created by chenli on 16/2/23.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "RCTBridgeModule.h"
#import "RCTViewManager.h"
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

@interface RCT_EXTERN_MODULE(Todo,NSObject)

RCT_EXTERN_METHOD(addEvent:(NSString *)name location:(NSString *)location)

RCT_EXTERN_METHOD(findSomeThing:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(findAll:(NSString *)name resolve:(RCTPromiseResolveBlock)resolve reject:(RCTPromiseRejectBlock)reject)

@end

//@interface RCT_EXTERN_MODULE(Newlabel,RCTViewManager)
//RCT_EXPORT_VIEW_PROPERTY(text, NSString *)
//@end