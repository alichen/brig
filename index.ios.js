/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Animated,
  NativeModules,
  NativeAppEventEmitter,
  requireNativeComponent
} from 'react-native';

import MyMapView from "./component/mymap";
import MyLabelView from "./component/mylabel";


class Brig extends Component {
  constructor(props){
    super(props);
    this.state = {
      bounceValue:new Animated.Value(0)      
    }
  }
  componentDidMount(){
    const {CalendarManager,Todo} = NativeModules
    CalendarManager.addEvent("alichen","changsha")
    Todo.addEvent("alichen","changsha")
    this.handleReminder = NativeAppEventEmitter.addListener("onReminder",(reminder)=>{
      // console.log(reminder)
    })
    this.handleOther = NativeAppEventEmitter.addListener("otherEvent",(ret)=>{
      // console.log(ret)
    })
    Todo.findSomeThing((ret)=>{
      // console.log('findSomeThing',ret)
    })
    // Todo.findAll("").then((ret)=>{
    //   console.log('findAll',ret)
    // }).catch((err)=>{
    //   console.log('findAll fail',err)
    // })
    // this.state.bounceValue.setValue(1.5)
    Animated.spring(this.state.bounceValue,{
      toValue:0.8,friction:1
    }).start()
  }
  componentWillUnmount(){
    this.handleReminder.remove()
    this.handleOther.remove()
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native1!
        </Text>
        <Text style={styles.instructions}>
          To get started, edit index.ios.js
        </Text>
        <Text style={styles.instructions}>
          Press Cmd+R to reload,{'\n'}
          Cmd+D or shake for dev menu
        </Text>
        <MyMapView style={styles.mymap}/>
        <MyLabelView style={styles.mylabel} text="hello world"/>
        <Animated.Image source={{uri:"http://i.imgur.com/XMKOH81.jpg'"}}
        style={{
          width:50,
          height:50,
          transform:[{
            scale:this.state.bounceValue
          }]
        }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  mymap:{
    width:250,
    height:250,
  },
  mylabel:{
    width:100,
    height:40,
    backgroundColor:"#FF3300"
  }
});

AppRegistry.registerComponent('Brig', () => Brig);
