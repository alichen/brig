'use strict';

import React,{
    Component,
    requireNativeComponent
} from "react-native";

class MyMapView extends Component{
    render(){
        return <MyMap {...this.props} />
    }
}

MyMapView.propTypes = {
    pitchEnabled:React.PropTypes.bool,
}


let MyMap = requireNativeComponent("MyMap",MyMapView)

module.exports = MyMapView