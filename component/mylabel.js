'use strict';

import React,{
    Component,
    requireNativeComponent
} from "react-native";

class MyLabelView extends Component{
    render(){
        return <MyLabel {...this.props} />
    }
}

MyLabelView.propTypes = {
    text:React.PropTypes.string,
}


let MyLabel = requireNativeComponent("MyLabel",MyLabelView)

module.exports = MyLabelView